1. To run a server locally, use Docker Compose (for Apple M1 processor change m1_latest in 211123353_docker-compose.yml). After that you can see documentation at http://localhost/docs/swagger.html.
2. After test running, you can get an Allure report using this script: allure serve target/surefire-reports/
