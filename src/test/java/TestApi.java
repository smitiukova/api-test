import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class TestApi {

    private Map<String, String> getHeaders(String username, String password) {

        Map<String, String> credentials = new HashMap<>();
        credentials.put("username", username);
        credentials.put("password", password);

        String token =
                given().
                        contentType(ContentType.JSON).
                        body(credentials).
                        when().
                        post("http://127.0.0.1:80/user/token").
                        jsonPath().
                        get("jwt");

        return new HashMap<>() {
            {
                put("Accept", "application/json");
                put("Authorization", "Bearer " + token);
            }
        };
    }

    private void sendRequest(Map<String, String> headers, Map<String, String> body, int httpStatus) {
        given().
                contentType(ContentType.JSON).
                headers(headers).
                body(body).
                when().
                post("http://127.0.0.1:80/product").
                then().
                statusCode(httpStatus);
    }

    @Test
    public void testRespondIsOkAdmin() {
        Map<String, String> body = new HashMap<>();
        body.put("name", "nameOfProduct");
        body.put("description", "productDescription");
        body.put("price", "10");

        sendRequest(getHeaders("admin", "admin"), body, 200);
    }

    @Test
    public void testRespondIsOkUser() {
        Map<String, String> body = new HashMap<>();
        body.put("name", "nameOfProduct");
        body.put("description", "productDescription");
        body.put("price", "10");

        sendRequest(getHeaders("user", "user"), body, 200);
    }

    @Test
    public void testRespondIs400() {
        Map<String, String> body = new HashMap<>();
        body.put("name", "nameOfProduct");
        body.put("description", "productDescription");
        body.put("price", "-10");

        sendRequest(getHeaders("admin", "admin"), body, 400);
    }

    @Test
    public void testRespondIs401() {
        Map<String, String> body = new HashMap<>();
        body.put("name", "nameOfProduct");
        body.put("description", "productDescription");
        body.put("price", "10");

        sendRequest(new HashMap<>(), body, 401);
    }
}
